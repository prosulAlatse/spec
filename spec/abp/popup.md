# Adblock Plus: Bubble UI

The bubble UI is the menu which is triggered when the Adblock Plus icon in the browser toolbar is clicked. The bubble UI displays notifications, top-level information on the extension usage and provides access to the extension's settings.

1. [General requirements](#general-requirements)
1. [Index](#index)
1. [Toolbar icon](#toolbar-icon)
1. [Notifications](#notifications)
1. [Style guide](#style-guide)
1. [Assets](#assets)

## General requirements

- Width: 340px

## Index

**General Design:** https://eyeogmbh.invisionapp.com/share/Q3THNN576PX#/screens/379344336

1. Logo image: [/res/abp/logo/ABP-logo-full.svg](/res/abp/logo/ABP-logo-full.svg)
1. Gear icon - Launches [Settings page](/spec/abp/desktop-settings/index.md) in a new tab (Refer to the [Style guide](#style-guide) for hover/click states)
1. [Subscription status](#subscription-status)
1. [Toggle](#toggle)
1. `Number of items blocked`
1. [Counter panel](#counter-panel)
1. [Report issue](#report-issue)
1. [Block element](#block-element)
1. [Footer teaser](#footer-teaser)

## Subscription status

**Design:** https://www.figma.com/file/mU4E9k2cSoKgUBEeke51yU/ABP-Premium?node-id=413%3A1808

We offer users the ability to subscribe to a premium version of the product, which would give them access to additional features. When opening up the bubble UI menu, depending on the plan they have, the user will see either a button that encourages them to upgrade, or one to notify them they have access to premium features.

### Free users

If the user is on the standard (free) plan, either because they never had a premium subscription or had one and it expired, they will see a button to upgrade. A subscription is considered _expired_ when it's  been canceled, either by the user's request or due to outdated billing information, and the billing cycle has ended.

- **Design:** https://www.figma.com/file/mU4E9k2cSoKgUBEeke51yU/ABP-Premium?node-id=413%3A1808
- **Button label text:** `Upgrade`
- **Link behind button:** `https://accounts.adblockplus.org/%LANG%/premium`

In the link, the `%LANG%` placeholder will be replaced by the locale code (e.g. `en_US`) and, if possible, should contain all of the following query parameters:

| Parameter | Description |
|-|-|
|`%ADDON_NAME%`|Extension name|
|`%ADDON_VERSION%`|Extension version|
|`%APPLICATION_NAME%`|Browser name|
|`%APPLICATION_VERSION%`|Browser version|
|`%PLATFORM_NAME%`|Browser engine name|
|`%PLATFORM_VERSION%`|Browser engine version|
|`%SOURCE%`|Equals `popup`|

### Premium users

If the user is on the premium plan, either because they have an _active_ subscription or a _canceled_ one that hasn't reached the end of the billing cycle, they will see a premium label instead of the `Upgrade` one.

- **Design:** https://www.figma.com/file/mU4E9k2cSoKgUBEeke51yU/ABP-Premium?node-id=413%3A1831
- **Label text:** `Premium`
- **Link behind button:** No link

## Toggle

![](/res/abp/bubble-ui/ABP-Enabled-Disabled.png)

The toggles to enable/disable the extension are only active on HTTP(S) sites, otherwise they are [inactive](#inactive) (i.e. an extension page).

### Enable/Disable behavior:

- Disabling the toggle on domain level also disables the toggle on page level.
- When the toggle on domain level is disabled, the page level toggle can not be enabled.
- Enabling the toggle on domain level also enables the toggle on page level.

### Enable/Disable text:
`Block ads on:`

`This website:`
`<exampledomain.com>`

`This page:`
`</examplepath>`

- Long domains are truncated with '...' at the end
- Only URL's path is shown for page level
  - URL's with more than one parameter show '...' at the end in place of parameter details: `example.com/test?…`

### Refresh

**Enable/Disable Design:**
https://eyeogmbh.invisionapp.com/share/TYSWLRTQ37D#/

- After enabling or disabling any of the toggles, the page needs to be reloaded for the setting to take effect.
- Refresh message is shown when toggles' state changes
  - Title: `Refresh this page`
  - Description: `Click the button below for changes to take effect.`
  - Button: `Refresh`
- Clicking `Refresh` reloads the webpage and closes the bubble UI.
- If changes to toggles are reverted before the page is reloaded, the refresh message disappears

## Inactive

**Design:** https://www.figma.com/file/8KDGWPTKK04X8nu0ZCsCLq/ABPUI_Share?node-id=0%3A1

**Illustration image:** [/res/abp/popup/assets/sloth-sleeping-illustration.svg](/res/abp/popup/assets/sloth-sleeping-illustration.svg)

The extension is inactive on non HTTP(S) sites.

Both the toggle panel and ad counter for "this page" are hidden.

An illustration and a message are displayed, so that the user doesn't perceive ABP as being broken.

**Text for the message:** `There's nothing to block on this page!`

## Counter panel

**Design:** https://eyeogmbh.invisionapp.com/share/A4TRD7REVFZ

**Assets:** [/res/abp/bubble-ui/assets/social-sharing/](/res/abp/bubble-ui/assets/social-sharing/)

**Section title:** Number of items blocked

- `<#> on this page`: `#` shows the number of blocked items by the extension on the current page. The panel is hidden on allowlisted websites and pages.
- `<#> in total` : `#` shows the total number of blocked items by the extension since it had been installed. The panel is hidden on private tabs, and we do not count the number of blocked ads in private tabs towards the total blocked number.
- `Share numbers with friends` : clicking on this shows the Facebook and Twitter icons, that allow the user to share the **total** amount of blocked items. The panel is hidden on private tabs.
  - `Cancel` reverts back to the `Share numbers with friends` message.
  - Facebook content:
      - Add link to: `https://adblockplus.org`
      - If blocked number is < 1000 show hashtag: `#AdblockPlus100`
      - If blocked number is < 10000 show hashtag: `#AdblockPlus1000`
      - If blocked number is < 100000 show hashtag: `#AdblockPlus10K`
      - If blocked number is < 1000000 show hashtag: `#AdblockPlus100K`
      - If blocked number is >= 10000000 show hashtag: `#AdblockPlus1M`
  - Twitter/Weibo content: `I've blocked XX ads with Adblock Plus. Give it a try - it's FREE! https://adblockplus.org/`, where `XX` uses the total number of blocked items.
  - Screen readers description:
    - Facebook icon - `Share on Facebook`
    - Twitter icon - `Share on Twitter`
    - Weibo icon - `Share on Weibo`
  - Only users with Chinese language settings have option to share also on Weibo

## Block element and Report issue

**Block element and Report issue design:** https://eyeogmbh.invisionapp.com/share/4CTE0RUW2BK

**Hover design:** https://eyeogmbh.invisionapp.com/share/Y2TE1L6P9R8

**Assets:** [/res/abp/popup/assets/](/res/abp/popup/assets/)

Block element button CTA: `Block element`, subtext: `Block specific element on this website`

Report issue button CTA: `Report an issue on this page`

### Block element

**Design:** https://eyeogmbh.invisionapp.com/public/share/KQXAE13Y6H7#/screens/417664367

`Block element` only appears on webpages when the extension is active and when there are detectable elements, otherwise this feature is inactive.

Process description:

- Click on 'Block element' 'Block specific element on this website' displays the message (in the Bubble UI) `Click an element on the page to block it.` This Bubble UI closes automatically after 5 seconds.
- [Design page 3](https://eyeogmbh.invisionapp.com/public/share/KQXAE13Y6H7#/screens/417664369) - Click `Cancel` to close the menu and return to the main bubble UI menu.
- After user click on the element, display the block element dialog box from [compser.md](/spec/abp/composer.md)

### Report issue

The button for launching the Issue Reporter is hidden in private tabs and only visible in non-private tabs when the extension is active on the page. The [Issue Reporter](/spec/abp/issue-reporter.md) is opened in a new tab.

For translations that run long, then stack the items for [report issue](#report-issue) and [block element](#block-element) into a single column.

## Footer teaser

Design: https://eyeogmbh.invisionapp.com/public/share/4E16T0NHYK

The footer teaser is shown to all users. Depending on the browser, some of the messages might be hidden. See section [_Footer teaser messages_](#footer-teaser-messages) to understand the target group of each message.

### Footer teaser messages

* The footer teaser will display different messages, in a specific order.
* All the links open in a new tab.
* The messages are translated per user's language.
* Some character limits need to be kept so that the messages won't mess the design:
    * Message: Max. 32 Characters
    * Call to action label: Max. 10 Characters
* Rotation:
    * When the Bubble UI is open, rotate to the next message after 3 seconds.
    * The user can switch between messages by clicking the bottom navigation menu.
    * The rotation stops once the user selects a specific message using the bottom navigation menu.
    * After the Bubble UI is closed, the rotation continues in the background[^rotation].
    * If the rotation is on and the user hovers the cursor over the footer, the rotation should stop and, if no message has been selected, resume once the cursor leaves the footer area. Similar behavior should be implemented when the footer is focused through keyboard navigation.
    * Animate the tab's waiting state when the rotation is on (e.g. animated progress bar).

| Order | Message | Call to action label | Link | Target group |
| - | - | - | - | - |
| 1 | `Interested in ABP on mobile?` | iOS and Android icons | [Documentation links](/spec/adblockplus.org/documentation-link.md): `%LINK%=adblock_browser_ios_store` & `%LINK%=adblock_browser_android_store` | All, except Microsoft Edge[^exception-edge]  |
| 2 | `Want to support ABP?` | `DONATE` | [Documentation link](/spec/adblockplus.org/documentation-link.md): `%LINK%=donate` | All |
| 3 | `Enjoying the extension?` | `RATE ABP` | Opens individual stores[^individual-stores] | All |
| 4 | `Stay connected with us!` | FB & Twitter icons | [Documentation links](/spec/adblockplus.org/documentation-link.md): `%LINK%=social_facebook` & `%LINK%=social_twitter` | All |
| 5 | `Having trouble using ABP?` | `GET HELP` | [Documentation link](/spec/adblockplus.org/documentation-link.md): `%LINK%=help_center` | All |

[^rotation]: The technical details of this are to be decided by the team. For the user it should feel as if the teaser footer's messages continue rotating so that when they will open the Bubble UI again, they will likely see a new message, depending on how much time has passed since the last open.

[^exception-edge]: Explicitly hide footer in Edge (no matter which engine) due MS Store restrictions ([see issue ticket](https://issues.adblockplus.org/ticket/7193)).

[^individual-stores]: Each store has it's own [documentation link](/spec/adblockplus.org/documentation-link.md). For Chrome it's `%LINK%=chrome_review`, for Firefox it's `%LINK%=firefox_review` and for Opera it's `%LINK%=opera_review`. [↩](#a2)

## Notifications

| Informational notification | Critical notification |
| ---- | ---- |
| ![](/res/abp/popup/info-notification.png) | ![](/res/abp/popup/critical-notification.png) |

### All notifications (except critical notifications)

1. Clicking 'Close' hides the current message.
1. Clicking 'Stop showing notifications' hides the current notification, prevents further notifications from showing up (except critical ones), and unchecks the "Show useful notifications" option.

### Critical notification

Clicking `Close` hides the current message.

## Toolbar icon

| Icon state | Description |
| --- | --- |
| ![](/res/abp/popup/toolbar-icon.png) | Extension enabled |
| ![](/res/abp/popup/abp-allowlisted.png) | Extension disabled |
| ![](/res/abp/popup/toolbar-icon-count.png) | Number of elements blocked on the active tab |
| ![](/res/abp/popup/abp-notification-critical.png) | Critical alert notification |
| ![](/res/abp/popup/abp-notification-information.png) | Informational notification |

## Style guide

All clickable elements (i.e. gear icon, report issue, block element, cancel) should have the following states:
1. **Active:** Dark grey
2. **Hover / On click / Selected:** Highlighted in blue
3. **Inactive:** (not always applicable) Light grey

## Assets

| Name | File |
| --- | --- |
| iconClose.svg | ![](/res/abp/popup/assets/iconClose.svg) |
| iconOff.svg | ![](/res/abp/popup/assets/iconOff.svg) |
| iconOn.svg | ![](/res/abp/popup/assets/iconOn.svg) |
| options.svg | ![](/res/abp/popup/assets/options.svg) |
| iconAndroid.svg | ![](/res/abp/popup/assets/iconAndroid.svg) |
| iconApple.svg | ![](/res/abp/popup/assets/iconApple.svg) |
| iconInfo.svg | ![](/res/abp/popup/assets/iconInfo.svg) |
| iconCritical.svg | ![](/res/abp/popup/assets/iconCritical.svg) |

More assets: [/res/abp/popup/assets/](/res/abp/popup/assets/)





